package com.bcgdv.dvlab.serivce.impl;

import com.bcgdv.dvlab.serivce.SampleService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Service
@Profile({"staging", "prod"})
public class SampleServiceImpl implements SampleService {
    @Value("${swagger.version}")
    private String version;

    @Override
    public String doSomething() {
        return "real-" + version;
    }
}
