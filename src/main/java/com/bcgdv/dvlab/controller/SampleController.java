package com.bcgdv.dvlab.controller;

import com.bcgdv.dvlab.serivce.SampleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@Api
@Slf4j
@RestController
public class SampleController {

    private SampleService sampleService;

    @Autowired
    public SampleController(SampleService sampleService ) {
        this.sampleService = sampleService;
    }

    @ApiOperation("测试")
    @GetMapping("/dosomething")
    public String doSomething() {
        log.info("doSomething.");
        return sampleService.doSomething();
    }
}
