package com.bcgdv.dvlab;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DVLabApplication {
    public static void main(String[] args) {
        SpringApplication.run(DVLabApplication.class);
    }
}
