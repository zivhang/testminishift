# 构建jar
FROM registry.cn-hangzhou.aliyuncs.com/junshuo/gradle5-jdk8 AS builder

ADD ./src src/
ADD ./build.gradle build.gradle

RUN gradle clean build

## 构建镜像
FROM registry.cn-hangzhou.aliyuncs.com/springcloud-cn/java:8u172-jre-alpine

ENV SPRING_PROFILES_ACTIVE prod

# TODO: remove version number
COPY --from=builder /home/gradle/build/libs/gradle-0.0.1.jar app.jar

RUN mkdir /log
VOLUME /log

ENTRYPOINT ["java", "-jar", "app.jar"]