# DV Lab
> DV Lab 项目后端

TODO:

## 开始

启动服务器

```shell
## 进入跟目录
cd <project_root>

## 构建并生成docker镜像
./gradlew run

```

如果服务工作正常，可以打开swagger-ui页面。
```shell
localhost:8000/swagger-ui.html
```

## 开发

- IntelliJ IDEA安装lombok插件。
  1. 打开插件安装界面： preferences -> plugins -> marketplace。
  2. 搜索插件 "lombok"。
  3. 点击"install"按钮安装。


### 构建

TODO:

### 部署与发布

TODO:

## 功能

TODO:

## 贡献者

TODO:

## 链接

- 小程序代码库：https://gitlab.com/dvshi/mp-dvlab

## 许可

All rights reserved by BCG DV.